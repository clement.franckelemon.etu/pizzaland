# API Pizzas

| URI | Opération | MIME | Requête | Réponse |
|:----|:---------:|:-----|:-------:|:--------:|
| /pizzas | GET | <-application/json | | Liste des pizzas (P1) |
| /pizzas/{id} | GET | <-application/json | | Une pizza (P1) ou 404 |
| /pizzas/{id}/nom | GET | <-text/plain | | Le nom de la pizza ou 404 |
| /pizzas/{id}/pate | GET | <-text/plain | | Le type de pâte de la pizza ou 404 |
| /pizzas/{id}/prixbase | GET | <-text/plain | | Le prix de la base de la pizza ou 404 |
| /pizzas/{id}/prixfinal | GET | <-application/json | | Le prix total de la pizza ou 404 |
| /pizzas/{id}/ingredients | GET | <-application/json | | La liste des ingredients de la pizza ou 404 |
| /pizzas | POST | <-/->application/json | Pizza (P2) | Nouvelle pizza (P1) 409 si la pizza existe déjà (même nom) |
| /pizzas/{id} | POST | <-/->application/json | Ingrédient (I2) | Ajout d'un ingédient à la pizza (P1) 409 si la pizza possède déjà l'ingrédient |
| /pizzas/{id} | PUT | <-/->application/json | Pizza (I2) | Pizza modifié (I1) 409 si la pizza existe déjà (si nom déjà utiliser) |
| /pizzas/{id} | PATCH | <-/->application/json | Pizza (I2) | Pizzas modifié (I1) 409 si la pizza existe déjà (si nom déjà utiliser) |
| /pizzas/{id} | DELETE | <-application/json | | Pizza supprimer (P1) 404 si la pizza n'existe pas |
| /pizzas/{id}/{id_ingredient} | DELETE | <-application/json | | Ingrédient retirer de la pizza (I1) 404 si la pizza ou l'ingredient n'existe pas |


## IMPORTANT !

**Toutes les requêtes hormis les requêtes de type GET nécéssite une authentification de type Bearer Token**, pour cela veuillez vous rediriger vers [l'API Users](API_Users.pdf).

Ce système d'authentification indiquera généralement le codes de status HTTP suivant :
| Status | Description |
|:------:|:------------|
|401 UNAUTHORIZED | Le token est manquant ou invalide |s

# Corps des requêtes

## P1

Une pizza comporte un identifiant, un nom, un type de pâte, un prix et une liste d'ingredients. Sa représentation JSON (P1) est la suivante :

```json
{
    "id": 1,
    "nom": "Ananas",
    "pate": "fine",
    "prixBase": 5,
    "ingredients": [
        {
            "id": 2,
            "name": "jambon",
            "prize": 3
        },
        {
            "id": 4,
            "name": "ananas",
            "prize": 4
        }
    ]
}
```

## P2

Lors de la création, l'identifiant n'est pas connu car il sera générer automatiquement. Aussi on aura une représentation JSON (P2) qui se compose ainsi :

```json
{
    "nom": "Royal",
    "pate": "epaise",
    "prixBase": 7,
    "ingredients": [
        {
            "id": 1,
            "name": "mozzarella",
            "prize": 2
        },
        {
            "id": 2,
            "name": "jambon",
            "prize": 3
        }
    ]
}
```

# Exemples

## Lister toutes les pizzas
> **GET** /pizzaland/pizzas

Requête vers le serveur
```json
GET /pizzaland/pizzas
```

Réponse du serveur
```json
[
    {
        "id": 1,
        "nom": "Ananas",
        "pate": "fine",
        "prixBase": 5,
        "ingredients": [
            {
                "id": 2,
                "name": "jambon",
                "prize": 3
            },
            {
                "id": 4,
                "name": "ananas",
                "prize": 4
            }
        ]
    },
    {
        "id": 2,
        "nom": "Reine",
        "pate": "epaise",
        "prixBase": 7,
        "ingredients": [
            {
                "id": 1,
                "name": "mozzarella",
                "prize": 2
            },
            {
                "id": 2,
                "name": "jambon",
                "prize": 3
            },
            {
                "id": 3,
                "name": "Champignion",
                "prize": 4
            }
        ]
    }
]
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |


## Récuperer une pizza
> **GET** /pizzaland/pizzas/{id}

Requête vers le serveur
```json
GET /pizzaland/pizzas/2
```

Réponse du serveur
```json
{
    "id": 2,
    "nom": "Reine",
    "pate": "epaise",
    "prixBase": 7,
    "ingredients": [
        {
            "id": 1,
            "name": "mozzarella",
            "prize": 2
        },
        {
            "id": 2,
            "name": "jambon",
            "prize": 3
        },
        {
            "id": 3,
            "name": "Champignion",
            "prize": 4
        }
    ]
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | La pizza n'existe pas |


## Récuperer le nom d'une pizza
> **GET** /pizzaland/pizzas/{id}/nom

Requête vers le serveur
```json
GET /pizzaland/pizzas/2/nom
```

Réponse du serveur
```json
"Reine"
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | La pizza n'existe pas |


## Récuperer le type de pâte d'une pizza
> **GET** /pizzaland/pizzas/{id}/pate

Requête vers le serveur
```json
GET /pizzaland/pizzas/2/pate
```

Réponse du serveur
```json
"epaise"
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | La pizza n'existe pas |


## Récuperer le prix de base d'une pizza
> **GET** /pizzaland/pizzas/{id}/prixbase

Requête vers le serveur
```json
GET /pizzaland/pizzas/2/prixbase
```

Réponse du serveur
```json
7
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | La pizza n'existe pas |


## Récuperer le prix final d'une pizza
> **GET** /pizzaland/pizzas/{id}/prixfinal

Requête vers le serveur
```json
GET /pizzaland/pizzas/2/prixfinal
```

Réponse du serveur
```json
16
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | La pizza n'existe pas |


## Récuperer les ingredients d'une pizza
> **GET** /pizzaland/pizzas/{id}/ingredients

Requête vers le serveur
```json
GET /pizzaland/pizza/2/ingredients
```

Réponse du serveur
```json
[
    {
        "id": 1,
        "name": "mozzarella",
        "prize": 2
    },
    {
        "id": 2,
        "name": "jambon",
        "prize": 3
    },
    {
        "id": 3,
        "name": "Champignion",
        "prize": 4
    }
]
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | La pizza n'existe pas |


## Ajouter une pizza
> **POST** /pizzaland/pizzas/

Requête vers le serveur
```json
POST /pizzaland/pizzas/
{
    "nom": "Royal",
    "pate": "epaise",
    "prixBase": 7,
    "ingredients": [
        {
            "id": 1,
            "name": "mozzarella",
            "prize": 2
        },
        {
            "id": 2,
            "name": "jambon",
            "prize": 3
        }
    ]
}
```

Réponse du serveur
```json
{
    "id": 3,
    "nom": "Royal",
    "pate": "epaise",
    "prixBase": 7,
    "ingredients": [
        {
            "id": 1,
            "name": "mozzarella",
            "prize": 2
        },
        {
            "id": 2,
            "name": "jambon",
            "prize": 3
        }
    ]
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|201 CREATED | La pizza a été ajouté |
|409 CONFLICT | Une pizza avec le même nom existe déjà |


## Ajouter un ingrédient à une pizza
> **POST** /pizzaland/pizzas/{id}

Requête vers le serveur
```json
POST /pizzaland/pizzas/1
{
    "id": 1,
    "name": "mozzarella",
    "prize": 2
}
```

Réponse du serveur
```json
{
    "id": 1,
    "name": "mozzarella",
    "prize": 2
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | L'ingrédient a été ajouté |
|409 CONFLICT | La pizza possède déjà l'ingrédient |


## Modifier une pizza
> **PUT** /pizzaland/pizzas/{id}

Requête vers le serveur
```json
GET /pizzaland/pizzas/2
{
    "nom": "Reine II",
    "pate": "fine",
    "ingredients": [
        {
            "name": "mozzarella",
        },
        {
            "name": "jambon",
            "prize": 3
        },
        {
            "name": "Ingredient_Secret",
            "prize": 17
        }
    ]
}
```

Réponse du serveur
```json
{
    "nom": "Reine II",
    "pate": "fine",
    "prixBase": 0,
    "ingredients": [
        {
            "id": 1,
            "name": "mozzarella",
            "prize": 2
        },
        {
            "id": 2,
            "name": "jambon",
            "prize": 3
        },
        {
            "id": 5,
            "name": "Ingredient_Secret",
            "prize": 17
        }
    ]
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La pizza a été totalement modifié |
|404 NOT FOUND | La pizza n'existe pas |


## Modifier une pizza
> **PATCH** /pizzaland/pizzas/{id}

Requête vers le serveur
```json
GET /pizzaland/pizzas/2
{
    "prixBase": 4
}
```

Réponse du serveur
```json
{
    "nom": "Reine II",
    "pate": "fine",
    "prixBase": 4,
    "ingredients": [
        {
            "id": 1,
            "name": "mozzarella",
            "prize": 2
        },
        {
            "id": 2,
            "name": "jambon",
            "prize": 3
        },
        {
            "id": 5,
            "name": "Ingredient_Secret",
            "prize": 17
        }
    ]
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La pizza a été partiellement modifié |
|404 NOT FOUND | La pizza n'existe pas |


## Supprimer un ingredient d'une pizza
> **DELETE** /pizzaland/pizzas/{id}/{id_ingredient}

Requête vers le serveur
```json
GET /pizzaland/pizzas/1/4
```

Réponse du serveur
```json
{
    "id": 4,
    "name": "ananas",
    "prize": 4
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | L'ingrédient a été retirer de la pizza |
|404 NOT FOUND | L'ingrédient ou la pizza n'existe pas |


## Supprimer une pizza
> **DELETE** /pizzaland/pizzas/{id}

Requête vers le serveur
```json
GET /pizzaland/pizzas/1
```

Réponse du serveur
```json
{
    "id": 1,
    "nom": "Ananas",
    "pate": "fine",
    "prixBase": 5,
    "ingredients": [
        {
            "id": 2,
            "name": "jambon",
            "prize": 3
        }
    ]
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | L'ingrédient a été retirer de la pizza |
|404 NOT FOUND | L'ingrédient ou la pizza n'existe pas |
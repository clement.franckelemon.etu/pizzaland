# API Users

| URI | Opération | MIME | Requête | Réponse |
|:---:|:---------:|:----:|:-------:|:--------:|
| /users/token | GET | <-application/json | | Récuperer un token de type Bearer Token (T1) |

# Corps des requêtes

## T1

La représentation du token en JSON (T1) est la suivante :

```json
"eyJhbGciOiJIUzM4NCJ9.eyJqdGkiOiJjODYyMjNjZThjYjM0Yjk4OGVlYzA3ZGNjNDI4NTE4OSIsImlhdCI6MTcxMDYyODM3OSwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHRwMzMzIiwiaXNzIjoicGhpbGlwcGUubWF0aGlldUB1bml2LWxpbGxlLmZyIiwiZXhwIjoxNzEwNjI5NTc5fQ.VnW40WXMLEQFFYmqcFkM6EJfRWv21_z13hMVHNNcI0dV9AzGgRIOnM1cqVRvP0QH"
```

# Exemples

## Lister toutes les commandes
> **GET** /pizzaland/users/token

**Cette commande nécéssite une Basic Token !**

Requête vers le serveur
```json
GET /pizzaland/users/token
Auth jean:jean
```

Réponse du serveur
```json
"eyJhbGciOiJIUzM4NCJ9.eyJqdGkiOiJjODYyMjNjZThjYjM0Yjk4OGVlYzA3ZGNjNDI4NTE4OSIsImlhdCI6MTcxMDYyODM3OSwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHRwMzMzIiwiaXNzIjoicGhpbGlwcGUubWF0aGlldUB1bml2LWxpbGxlLmZyIiwiZXhwIjoxNzEwNjI5NTc5fQ.VnW40WXMLEQFFYmqcFkM6EJfRWv21_z13hMVHNNcI0dV9AzGgRIOnM1cqVRvP0QH"
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|401 UNAUTHORIZED | L'utilisateur n'a pas réussi à s'authentifié |

# API Ingredients

| URI | Opération | MIME | Requête | Réponse |
|:---:|:---------:|:----:|:-------:|:--------:|
| /ingredients | GET | <-application/json | | Liste des ingredients (l1) |
| /ingredients/{id} | GET | <-application/json | | Un ingrédient (I1) ou 404 |
| /ingredients/{id}/nom | GET | <-text/plain | | Le nom de l'ingrédient ou 404 |
| /ingredients/{id}/prix | GET | <-text/plain | | Le prix de l'ingrédient ou 404 |
| /ingredients | POST | <-/->application/json | Ingrédient (I2) | Nouvel ingrédient (I1) 409 si l'ingrédient existe déjà (même nom) |
| /ingredients/{id} | PUT | <-/->application/json | Ingrédient (I2) | Ingrédient modifié (I1) 409 si l'ingrédient existe déjà (si nom déjà utiliser) |
| /ingredients/{id} | PATCH | <-/->application/json | Ingrédient (I2) | Ingrédient modifié (I1) 409 si l'ingrédient existe déjà (si nom déjà utiliser) |
| /ingredients/{id} | DELETE | <-application/json | | Ingrédient supprimer (I1) 404 si l'ingrédient n'existe pas |


## IMPORTANT !

**Toutes les requêtes hormis les requêtes de type GET nécéssite une authentification de type Bearer Token**, pour cela veuillez vous rediriger vers [l'API Users](API_Users.pdf).

Ce système d'authentification indiquera généralement le codes de status HTTP suivant :
| Status | Description |
|:------:|:------------|
|401 UNAUTHORIZED | Le token est manquant ou invalide |

# Corps des requêtes

## I1

Un ingrédient comporte uniquement un identifiant, un nom et un prix. Sa représentation JSON (I1) est la suivante :

```json
{
    "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
    "name": "mozzarella",
    "prize": 2
}
```

## I2

Lors de la création, l'identifiant n'est pas connu car il sera générer automatiquement. Aussi on aura une représentation JSON (I2) qui comporte uniquement le nom et prix:

```json
{
    "name": "mozzarella",
    "prize": 2
}
```

# Exemples

## Lister tous les ingredients
> **GET** /pizzaland/ingredients

Requête vers le serveur
```json
GET /pizzaland/ingredients
```

Réponse du serveur
```json
[
    {
        "id": 1,
        "name": "mozzarella",
        "prize": 2
    },
    {
        "id": 2,
        "name": "jambon",
        "prize": 3
    }
]
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |


## Récuperer un ingredients
> **GET** /pizzaland/ingredients/{id}

Requête vers le serveur
```json
GET /pizzaland/ingredients/2
```

Réponse du serveur
```json
{
    "id": 2,
    "name": "jambon",
    "prize": 3
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | L'ingrédient n'existe pas |


## Récuperer le nom d'un ingredients
> **GET** /pizzaland/ingredients/{id}/nom

Requête vers le serveur
```json
GET /pizzaland/ingredients/2/nom
```

Réponse du serveur
```json
"jambon"
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | L'ingrédient n'existe pas |


## Récuperer le prix d'un ingredients
> **GET** /pizzaland/ingredients/{id}/prix

Requête vers le serveur
```json
GET /pizzaland/ingredients/2/prix
```

Réponse du serveur
```json
3
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | L'ingrédient n'existe pas |


## Ajouter un ingrdient
> **POST** /pizzaland/ingredients/

Requête vers le serveur
```json
POST /pizzaland/ingredients/
{
    "name": "Champignion",
    "prize": 1
}
```

Réponse du serveur
```json
{
    "id": 3,
    "name": "Champignion",
    "prize": 1
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|201 CREATED | L'ingrédient a été ajouté |
|409 CONFLICT | Un ingrédient avec le même nom existe déjà |


## Modifier un ingredient
> **PUT** /pizzaland/ingredients/{id}

Requête vers le serveur
```json
GET /pizzaland/ingredients/3
{
    "name": "Champignions"
}
```

Réponse du serveur
```json
{
    "id": 3,
    "name": "Champignion",
    "prize": 0
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | L'ingrédient a été totalement modifié |
|404 NOT FOUND | L'ingrédient n'existe pas |


## Modifier un ingredient
> **PATCH** /pizzaland/ingredients/{id}

Requête vers le serveur
```json
GET /pizzaland/ingredients/3
{
    "prize": 4
}
```

Réponse du serveur
```json
{
    "id": 3,
    "name": "Champignion",
    "prize": 4
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | L'ingrédient a été partiellement modifié |
|404 NOT FOUND | L'ingrédient n'existe pas |


## Supprimer un ingredient
> **DELETE** /pizzaland/ingredients/{id}

Requête vers le serveur
```json
GET /pizzaland/ingredients/3
```

Réponse du serveur
```json
{
    "id": 3,
    "name": "Champignions",
    "prize": 4
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | L'ingrédient a été supprimé |
|404 NOT FOUND | L'ingrédient n'existe pas |
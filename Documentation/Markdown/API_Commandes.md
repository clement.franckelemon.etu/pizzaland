# API Commandes

| URI | Opération | MIME | Requête | Réponse |
|:---:|:---------:|:----:|:-------:|:--------:|
| /commandes | GET | <-application/json | | Liste des commandes (C1) |
| /commandes/{id} | GET | <-application/json | | Une commande (C1) ou 404 |
| /commandes/{id}/utilisateur | GET | <-text/plain | | Le nom de l'utilisateur de la commande ou 404 |
| /commandes/{id}/prixfinal | GET | <-text/plain | | Le prix total de la commande ou 404 |
| /commandes/{id}/jourcreer | GET | <-text/plain | | La date de création de la commande ou 404 |
| /commandes/{id}/pizzas | GET | <-text/plain | | La liste des pizzas de la commande ou 404 |
| /commandes | POST | <-/->application/json | Commande (C2) | Nouvelle commande (C1) |


## IMPORTANT !

**Toutes les requêtes hormis les requêtes de type GET nécéssite une authentification de type Bearer Token**, pour cela veuillez vous rediriger vers [l'API Users](API_Users.pdf).

Ce système d'authentification indiquera généralement le codes de status HTTP suivant :
| Status | Description |
|:------:|:------------|
|401 UNAUTHORIZED | Le token est manquant ou invalide |

# Corps des requêtes

## C1

Un ingrédient comporte uniquement un identifiant, le nom de l'utilisateur, la jour de création et la liste des pizzas. Sa représentation JSON (C1) est la suivante :

```json
{
    "id": 1,
    "userName": "Toto",
    "dayCreated": "24-02-2024",
    "pizzas": [
        {
            "id": 3,
            "nom": "Royal",
            "pate": "epaise",
            "prixBase": 7,
            "ingredients": [
                {
                    "id": 1,
                    "name": "mozzarella",
                    "prize": 2
                },
                {
                    "id": 2,
                    "name": "jambon",
                    "prize": 3
                }
            ]
        }
    ]
}
```

## C2

Lors de la création, l'identifiant n'est pas connu car il sera générer automatiquement. Aussi on aura une représentation JSON (C2) qui comporte uniquement le nom de l'utilisateur, jour de création et la liste des pizzas composé uniquement de leur noms:

```json
{
    "userName": "Toto",
    "dayCreated": "24-02-2024",
    "pizzas": [
        {
            "nom": "Royal"
        },
        {
            "nom": "Royal"
        },
        {
            "nom": "Ananas"
        }
    ]
}
```

# Exemples

## Lister toutes les commandes
> **GET** /pizzaland/commandes

Requête vers le serveur
```json
GET /pizzaland/commandes
```

Réponse du serveur
```json
[
    {
        "id": 1,
        "userName": "Toto",
        "dayCreated": "24-02-2024",
        "pizzas": [
            {
                "id": 3,
                "nom": "Royal",
                "pate": "epaise",
                "prixBase": 7,
                "ingredients": [
                    {
                        "id": 1,
                        "name": "mozzarella",
                        "prize": 2
                    },
                    {
                        "id": 2,
                        "name": "jambon",
                        "prize": 3
                    }
                ]
            }
        ]
    },
    {
        "id": 2,
        "userName": "Tata",
        "dayCreated": "25-02-2024",
        "pizzas": [
            {
                "id": 3,
                "nom": "Royal",
                "pate": "epaise",
                "prixBase": 7,
                "ingredients": [
                    {
                        "id": 1,
                        "name": "mozzarella",
                        "prize": 2
                    },
                    {
                        "id": 2,
                        "name": "jambon",
                        "prize": 3
                    }
                ]
            },
            {
                "id": 1,
                "nom": "Ananas",
                "pate": "fine",
                "prixBase": 5,
                "ingredients": [
                    {
                        "id": 2,
                        "name": "jambon",
                        "prize": 3
                    },
                    {
                        "id": 4,
                        "name": "ananas",
                        "prize": 4
                    }
                ]
            },
            {
                "id": 1,
                "nom": "Ananas",
                "pate": "fine",
                "prixBase": 5,
                "ingredients": [
                    {
                        "id": 2,
                        "name": "jambon",
                        "prize": 3
                    },
                    {
                        "id": 4,
                        "name": "ananas",
                        "prize": 4
                    }
                ]
            }
        ]
    }
]
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |


## Récuperer une commande
> **GET** /pizzaland/commandes/{id}

Requête vers le serveur
```json
GET /pizzaland/commandes/2
```

Réponse du serveur
```json
{
    "id": 2,
    "userName": "Tata",
    "dayCreated": "25-02-2024",
    "pizzas": [
        {
            "id": 3,
            "nom": "Royal",
            "pate": "epaise",
            "prixBase": 7,
            "ingredients": [
                {
                    "id": 1,
                    "name": "mozzarella",
                    "prize": 2
                },
                {
                    "id": 2,
                    "name": "jambon",
                    "prize": 3
                }
            ]
        },
        {
            "id": 1,
            "nom": "Ananas",
            "pate": "fine",
            "prixBase": 5,
            "ingredients": [
                {
                    "id": 2,
                    "name": "jambon",
                    "prize": 3
                },
                {
                    "id": 4,
                    "name": "ananas",
                    "prize": 4
                }
            ]
        },
        {
            "id": 1,
            "nom": "Ananas",
            "pate": "fine",
            "prixBase": 5,
            "ingredients": [
                {
                    "id": 2,
                    "name": "jambon",
                    "prize": 3
                },
                {
                    "id": 4,
                    "name": "ananas",
                    "prize": 4
                }
            ]
        }
    ]
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | La commande n'existe pas |


## Récuperer le prix final d'une commande
> **GET** /pizzaland/commandes/{id}/prixfinal

Requête vers le serveur
```json
GET /pizzaland/commandes/2/prixfinal
```

Réponse du serveur
```json
36
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | La commande n'existe pas |


## Récuperer le nom de l'utilisateur d'une commande
> **GET** /pizzaland/commandes/{id}/utilisateur

Requête vers le serveur
```json
GET /pizzaland/commandes/2/utilisateur
```

Réponse du serveur
```json
"Tata"
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | La commande n'existe pas |


## Récuperer la date de création d'une commande
> **GET** /pizzaland/commandes/{id}/jourcreer

Requête vers le serveur
```json
GET /pizzaland/commandes/2/jourcreer
```

Réponse du serveur
```json
"25-02-2024"
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | La commande n'existe pas |


## Récuperer la liste des pizzas d'une commande
> **GET** /pizzaland/commandes/{id}/pizzas

Requête vers le serveur
```json
GET /pizzaland/commandes/2/pizzas
```

Réponse du serveur
```json
[
    {
        "id": 3,
        "nom": "Royal",
        "pate": "epaise",
        "prixBase": 7,
        "ingredients": [
            {
                "id": 1,
                "name": "mozzarella",
                "prize": 2
            },
            {
                "id": 2,
                "name": "jambon",
                "prize": 3
            }
        ]
    },
    {
        "id": 1,
        "nom": "Ananas",
        "pate": "fine",
        "prixBase": 5,
        "ingredients": [
            {
                "id": 2,
                "name": "jambon",
                "prize": 3
            },
            {
                "id": 4,
                "name": "ananas",
                "prize": 4
            }
        ]
    },
    {
        "id": 1,
        "nom": "Ananas",
        "pate": "fine",
        "prixBase": 5,
        "ingredients": [
            {
                "id": 2,
                "name": "jambon",
                "prize": 3
            },
            {
                "id": 4,
                "name": "ananas",
                "prize": 4
            }
        ]
    }
]
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|200 OK | La requête s'est effectuée correctement |
|404 NOT FOUND | La commande n'existe pas |


## Ajouter une commande
> **POST** /pizzaland/commandes/

Requête vers le serveur
```json
POST /pizzaland/commandes/
{
    "userName": "Toto",
    "dayCreated": "26-02-2024",
    "pizzas": [
        {
            "nom": "Royal"
        }
    ]
}
```

Réponse du serveur
```json
{
    "id": 3,
    "userName": "Toto",
    "dayCreated": "26-02-2024",
    "pizzas": [
        {
            "id": 3,
            "nom": "Royal",
            "pate": "epaise",
            "prixBase": 7,
            "ingredients": [
                {
                    "id": 1,
                    "name": "mozzarella",
                    "prize": 2
                },
                {
                    "id": 2,
                    "name": "jambon",
                    "prize": 3
                }
            ]
        }
    ]
}
```

Codes de status HTTP
| Status | Description |
|:------:|:------------|
|201 CREATED | La commande a été ajouté |
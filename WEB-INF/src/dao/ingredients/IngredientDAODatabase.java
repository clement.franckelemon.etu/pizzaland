package dao.ingredients;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dao.DS;
import dto.Ingredient;

public class IngredientDAODatabase implements DAOIngredient {

    @Override
    public List<Ingredient> findAll() {
        List<Ingredient> all = new ArrayList<>();
        try(Connection con = DS.getConnection()){
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM ingredients;");
            while (rs.next()) {
                all.add(new Ingredient(rs.getInt(1),rs.getString(2),rs.getInt(3)));
            }
            return all;
        } catch (SQLException e) {
            return all;
        }
    }

    @Override
    public Ingredient findById(int id) {
        try(Connection con = DS.getConnection()){
            Ingredient ingredient = null;
            PreparedStatement ps = con.prepareStatement("SELECT * FROM ingredients WHERE ino=? ;");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                ingredient = new Ingredient(rs.getInt(1),rs.getString(2),rs.getInt(3));
            }
            return ingredient;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public String findNameById(int id) {
        try(Connection con = DS.getConnection()){
            String nomIngredient = null;
            PreparedStatement ps = con.prepareStatement("SELECT nom FROM ingredients WHERE ino=? ;");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                nomIngredient = rs.getString(1);
            }
            return nomIngredient;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public Integer findPrizeById(int id) {
        try(Connection con = DS.getConnection()){
            Integer ingredientPrize = null;
            PreparedStatement ps = con.prepareStatement("SELECT prix FROM ingredients WHERE ino=? ;");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                ingredientPrize = rs.getInt(1);
            }
            return ingredientPrize;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public boolean save(Ingredient ingredient) {
        if(!searchName(ingredient.getName())){
            try(Connection con = DS.getConnection()){
                PreparedStatement ps = con.prepareStatement("INSERT INTO ingredients(nom,prix) VALUES (?,?) RETURNING ino;");
                ps.setString(1, ingredient.getName());
                ps.setInt(2, ingredient.getPrize());
                ResultSet rs = ps.executeQuery();
                if(rs.next()){
                    int generatedKey = rs.getInt(1);
                    ingredient.setId(generatedKey);
                    return true;
                } else {
                    return false;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean searchName(String name) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT ino FROM ingredients WHERE nom=? ;");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            return false;
        }
    }

    private boolean search(String name,int id) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT ino FROM ingredients WHERE nom=? AND ino=? ;");
            ps.setString(1, name);
            ps.setInt(2, id);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean deleteById(int id) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("DELETE FROM ingredients WHERE ino=? ;");
            ps.setInt(1, id);
            int rs = ps.executeUpdate();
            return rs == 1;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean modifAllById(int id, Ingredient ingredient) {
        if(search(ingredient.getName(), id) || !searchName(ingredient.getName())){
            try(Connection con = DS.getConnection()){
                PreparedStatement ps = con.prepareStatement("UPDATE ingredients SET nom=?, prix=? WHERE ino=? ;");
                ps.setString(1,ingredient.getName());
                ps.setInt(2,ingredient.getPrize());
                ps.setInt(3,id);
                int rs = ps.executeUpdate();
                return rs == 1;
            } catch (SQLException e) {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean modifById(int id, Ingredient ingredient) {
        if(search(ingredient.getName(), id) || !searchName(ingredient.getName())){
            try(Connection con = DS.getConnection()){
                String set = "";
                if(ingredient.getName() != null && !ingredient.getName().isEmpty()){
                    set += "nom=?";
                }
                if(ingredient.getPrize() != 0){
                    if(!set.isEmpty()) set += ",";
                    set += "prix=?";
                }
                PreparedStatement ps = con.prepareStatement("UPDATE ingredients SET "+set+" WHERE ino=? ;");
                int x = 1;
                if(ingredient.getName() != null && !ingredient.getName().isEmpty()){
                    ps.setString(x, ingredient.getName());
                    x++;
                }
                if(ingredient.getPrize() != 0){
                    ps.setInt(x, ingredient.getPrize());
                    x++;
                }
                ps.setInt(x, id);
                int rs = ps.executeUpdate();
                return rs == 1;
            } catch (SQLException e) {
                return false;
            }
        } else{
            return false;
        }
    }
}

package dao.ingredients;

import java.util.List;

import dto.Ingredient;

public interface DAOIngredient {
    
    public List<Ingredient> findAll();

    public Ingredient findById(int id);
    
    public boolean save(Ingredient ingredient);

    public String findNameById(int id);

    public Integer findPrizeById(int id);

    public boolean deleteById(int id);

    public boolean modifById(int id, Ingredient ingredient);

    public boolean modifAllById(int id, Ingredient ingredient);

}

package dao.users;

import dto.User;

public interface DAOUser {
    
    public boolean userExists(User user);

    public boolean addToken(User user);

    public boolean tokenExists(String token);

    public User userOfThisToken(String token);

}
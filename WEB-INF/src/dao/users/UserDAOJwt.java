package dao.users;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Date;

import dao.DS;
import dto.User;
import io.jsonwebtoken.Claims;

public class UserDAOJwt implements DAOUser{

    @Override
    public boolean userExists(User user) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT nom FROM utilisateurs WHERE nom=? AND mdp=? ;");
            ps.setString(1, user.getNom());
            ps.setString(2, user.getMdp());
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean addToken(User user) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("UPDATE utilisateurs SET token=? WHERE nom=? AND mdp=? ;");
            user.setToken(JwtManager.createJWT());
            ps.setString(1, user.getToken());
            ps.setString(2, user.getNom());
            ps.setString(3, user.getMdp());
            int rs = ps.executeUpdate();
            return rs == 1;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean tokenExists(String token) {
        try(Connection con = DS.getConnection()){
            Claims claims = JwtManager.decodeJWT(token);
            if(claims.getExpiration().before(Date.from(Instant.now()))){
                return false;
            }
            PreparedStatement ps = con.prepareStatement("SELECT nom FROM utilisateurs WHERE token=? ;");
            ps.setString(1, token);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public User userOfThisToken(String token) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT nom,mdp FROM utilisateurs WHERE token=? ;");
            ps.setString(1, token);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return new User(rs.getString(1), rs.getString(2), token);
            }
            return null;
        } catch (SQLException e) {
            return null;
        }
    }
    
}

package dao;

import dao.commandes.CommandesDAODatabase;
import dao.commandes.DAOCommandes;
import dao.ingredients.DAOIngredient;
import dao.ingredients.IngredientDAODatabase;
import dao.pizzas.DAOPizza;
import dao.pizzas.PizzaDAODataBase;
import dao.users.DAOUser;
import dao.users.UserDAOJwt;

public class DAOFactory {
    
    public static DAOIngredient createDAOIngredients(){
        return new IngredientDAODatabase();
    }

    public static DAOPizza createDAOPizza(){
        return new PizzaDAODataBase();
    }

    public static DAOCommandes createDAOCommandes(){
        return new CommandesDAODatabase();
    }

    public static DAOUser createDAOUser(){
        return new UserDAOJwt();
    }

}

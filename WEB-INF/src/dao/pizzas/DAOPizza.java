package dao.pizzas;

import java.util.List;

import dto.Ingredient;
import dto.Pizza;

public interface DAOPizza {
    
    public List<Pizza> findAll();

    public Pizza findById(int id);

    public String findNameById(int id);

    public String findDoughById(int id);

    public Integer findPrizeById(int id);

    public List<Ingredient> findIngredientsById(int id);

    public Integer findFinalPrizeById(int id);

    public boolean save(Pizza pizza);

    public boolean deleteById(int id);

    public boolean modifById(int id, Pizza pizza);

    public boolean modifAllById(int id, Pizza pizza);

    public boolean addIngredientById(int id, Ingredient ingredient);

    public boolean removeIngredientById(int id, int idIngredient);

}

package dao.pizzas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dao.DS;
import dto.Ingredient;
import dto.Pizza;

public class PizzaDAODataBase implements DAOPizza {

    @Override
    public List<Pizza> findAll() {
        List<Pizza> all = new ArrayList<>();
        try(Connection con = DS.getConnection()){
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM pizzas;");
            Pizza pizza;
            while (rs.next()) {
                pizza = new Pizza(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
                pizza.addAllIngredients(findIngredientsById(pizza.getId()));
                all.add(pizza);
            }
            return all;
        } catch (SQLException e) {
            return all;
        }
    }

    @Override
    public String findNameById(int id) {
        try(Connection con = DS.getConnection()){
            String name = null;
            PreparedStatement ps = con.prepareStatement("SELECT nom FROM pizzas WHERE pno=? ;");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                name = rs.getString(1);
            }
            return name;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public String findDoughById(int id) {
        try(Connection con = DS.getConnection()){
            String dough = null;
            PreparedStatement ps = con.prepareStatement("SELECT pate FROM pizzas WHERE pno=? ;");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                dough = rs.getString(1);
            }
            return dough;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public Integer findPrizeById(int id) {
        try(Connection con = DS.getConnection()){
            Integer prize = null;
            PreparedStatement ps = con.prepareStatement("SELECT prix_base FROM pizzas WHERE pno=? ;");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                prize = rs.getInt(1);
            }
            return prize;
        } catch (SQLException e) {
            return null;
        }
    }

    public List<Ingredient> findIngredientsById(int id) {
        List<Ingredient> all = new ArrayList<>();
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT ingredients.ino,nom,prix FROM ingredients JOIN ingredients_par_Pizzas ON(ingredients.ino = ingredients_par_Pizzas.ino) WHERE pno=?;");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            Ingredient ingredient;
            while (rs.next()) {
                ingredient = new Ingredient(rs.getInt(1),rs.getString(2),rs.getInt(3));
                all.add(ingredient);
            }
            if (all.size() == 0 && findById(id) == null){                
                return null;
            } else {
                return all;
            }
        } catch (SQLException e) {
            return all;
        }
    }

    @Override
    public Pizza findById(int id) {
        try(Connection con = DS.getConnection()){
            Pizza pizza = null;
            PreparedStatement ps = con.prepareStatement("SELECT * FROM pizzas WHERE pno=? ;");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                pizza = new Pizza(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
                pizza.addAllIngredients(findIngredientsById(id));
            }
            return pizza;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public Integer findFinalPrizeById(int id) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT prix_base+(SELECT SUM(prix) FROM ingredients JOIN ingredients_par_Pizzas ON(ingredients.ino = ingredients_par_Pizzas.ino) WHERE pno=?) FROM pizzas WHERE pno=? ;");
            ps.setInt(1, id);
            ps.setInt(2, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return rs.getInt(1);
            }
            return null;
        } catch (SQLException e) {
            return null;
        }
    }


    private boolean searchPizza(String name,int id) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT prix_base FROM pizzas WHERE nom=? AND pno=? ;");
            ps.setString(1, name);
            ps.setInt(2, id);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            return false;
        }
    }


    private boolean searchNamePizza(String name) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT pno FROM pizzas WHERE nom=? ;");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            return false;
        }
    }


    private boolean searchNameIngredient(String name) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT ino FROM ingredients WHERE nom=? ;");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            return false;
        }
    }


    @Override
    public boolean save(Pizza pizza) {
        if (!searchNamePizza(pizza.getNom())){
            try(Connection con = DS.getConnection()){
                PreparedStatement ps = con.prepareStatement("INSERT INTO pizzas(nom,pate,prix_base) VALUES (?,?,?) RETURNING pno ;");
                ps.setString(1, pizza.getNom());
                ps.setString(2, pizza.getPate());
                ps.setInt(3, pizza.getPrixBase());
                ResultSet rs = ps.executeQuery();
                if(rs.next()){
                    int generatedKey = rs.getInt(1);
                    pizza.setId(generatedKey);
                    return saveIngredients(pizza.getId(),pizza.getIngredients());
                } else {
                    return false;
                }
            } catch (SQLException e) {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean saveIngredient(Ingredient ingredient){
        if (!searchNameIngredient(ingredient.getName())){
            try(Connection con = DS.getConnection()){
                PreparedStatement ps = con.prepareStatement("INSERT INTO ingredients(nom,prix) VALUES (?,?) RETURNING ino ;");
                ps.setString(1, ingredient.getName());
                ps.setInt(2, ingredient.getPrize());
                ResultSet rs = ps.executeQuery();
                if(rs.next()){
                    int generatedKey = rs.getInt(1);
                    ingredient.setId(generatedKey);
                    return true;
                }
                return false;
            } catch (SQLException e) {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean saveIngredients(int id,List<Ingredient> ingredients){
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("INSERT INTO ingredients(nom,prix) VALUES (?,?) RETURNING ino ;");
            for(Ingredient ingredient : ingredients){
                if (!searchNameIngredient(ingredient.getName())){
                    try{
                        ps.setString(1, ingredient.getName());
                        ps.setInt(2, ingredient.getPrize());
                        ResultSet rs = ps.executeQuery();
                        if(rs.next()){
                            int generatedKey = rs.getInt(1);
                            ingredient.setId(generatedKey);
                        }
                    } catch (SQLException e){
                        continue;
                    } finally{
                        ps.clearParameters();
                    }
                } else {
                    Ingredient verifIngredient = searchIngredient(ingredient.getName());
                    if (verifIngredient != null){
                        ingredient.setId(verifIngredient.getId());
                        ingredient.setPrize(verifIngredient.getPrize());
                    }
                }
            }
            return saveIngredientsOfPizzas(id, ingredients);
        } catch (SQLException e) {
            return false;
        }
    }

    private Ingredient searchIngredient(String name) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT ino,prix FROM ingredients WHERE nom=? ;");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return new Ingredient(rs.getInt(1),name,rs.getInt(2));
        } catch (SQLException e) {
            return null;
        }
    }

    private boolean saveIngredientsOfPizzas(int id,List<Ingredient> ingredients){
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("INSERT INTO ingredients_par_pizzas(pno,ino) VALUES (?,?) ;");
            for(Ingredient ingredient : ingredients){
                try{
                    ps.setInt(1, id);
                    ps.setInt(2, ingredient.getId());
                    ps.executeUpdate();
                } catch (SQLException e){
                    continue;
                } finally{
                    ps.clearParameters();
                }
            }
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public boolean deleteById(int id) {
        removeAllIngredientsOfPizza(id);
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("DELETE FROM pizzas WHERE pno=? ;");
            ps.setInt(1, id);
            int rs = ps.executeUpdate();
            return rs == 1;
        } catch (SQLException e) {
            return false;
        }
    }

    private void removeAllIngredientsOfPizza(int id){
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("DELETE FROM ingredients_par_pizzas WHERE pno=?");
            ps.setInt(1, id);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean modifById(int id,Pizza pizza) {
        if (searchPizza(pizza.getNom(),id) || !searchNamePizza(pizza.getNom())){
            try(Connection con = DS.getConnection()){
                String set = "";
                if(pizza.getNom() != null && !pizza.getNom().isEmpty()){
                    set += "nom=?";
                }
                if(pizza.getPate() != null && !pizza.getPate().isEmpty()){
                    if(!set.isEmpty()) set += ",";
                    set += "pate=?";
                }
                if(pizza.getPrixBase() != -1){
                    if(!set.isEmpty()) set += ",";
                    set += "prix_base=?";
                }
                PreparedStatement ps = con.prepareStatement("UPDATE pizzas SET "+set+" WHERE pno=? ;");
                int x = 1;
                if(pizza.getNom() != null && !pizza.getNom().isEmpty()){
                    ps.setString(x, pizza.getNom());
                    x++;
                }
                if(pizza.getPate() != null && !pizza.getPate().isEmpty()){
                    ps.setString(x, pizza.getPate());
                    x++;
                }
                if(pizza.getPrixBase() != -1){
                    ps.setInt(x, pizza.getPrixBase());
                    x++;
                }
                ps.setInt(x, id);
                int rs = ps.executeUpdate();
                return rs == 1;
            } catch (SQLException e) {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean modifAllById(int id, Pizza pizza) {
        if (searchPizza(pizza.getNom(),id) || !searchNamePizza(pizza.getNom())){
            try(Connection con = DS.getConnection()){
                PreparedStatement ps = con.prepareStatement("UPDATE pizzas SET nom=?, pate=?, prix_base=? WHERE pno=? ;");
                ps.setString(1,pizza.getNom());
                ps.setString(2,pizza.getPate());
                ps.setInt(3,pizza.getPrixBase());
                ps.setInt(4,id);
                int rs = ps.executeUpdate();
                removeAllIngredientsOfPizza(id);
                addAllIngredientsById(id, pizza.getIngredients());
                return rs == 1;
            } catch (SQLException e) {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean addIngredientById(int id, Ingredient ingredient) {
        if (!saveIngredient(ingredient)){
            Ingredient verifIngredient = searchIngredient(ingredient.getName());
            if (verifIngredient != null){
                ingredient.setId(verifIngredient.getId());
                ingredient.setPrize(verifIngredient.getPrize());
            }
        }
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("INSERT INTO ingredients_par_pizzas(pno,ino) VALUES (?,?) ;");
            ps.setInt(1, id);
            ps.setInt(2, ingredient.getId());
            int rs = ps.executeUpdate();
            return rs == 1;
        } catch (SQLException e) {
            return false;
        }
    }

    public void addAllIngredientsById(int id, List<Ingredient> ingredients){
        for (Ingredient ingredient : ingredients){
            addIngredientById(id, ingredient);
        }
    }

    @Override
    public boolean removeIngredientById(int id, int idIngredient) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("DELETE FROM ingredients_par_pizzas WHERE pno=? AND ino=? ;");
            ps.setInt(1, id);
            ps.setInt(2, idIngredient);
            int rs = ps.executeUpdate();
            return rs == 1;
        } catch (SQLException e) {
            return false;
        }
    }
    
}

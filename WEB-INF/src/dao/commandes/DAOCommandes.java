package dao.commandes;

import dto.Commande;
import dto.Pizza;

import java.sql.Date;
import java.util.List;

public interface DAOCommandes {
    
    public List<Commande> findAll();

    public Commande findById(int id);

    public String findUserById(int id);

    public Date findDateById(int id);

    public List<Pizza> findPizzasById(int id);

    public Integer findFinalPrizeById(int id);

    public boolean save(Commande commande);

}

package dao.commandes;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.Map.Entry;

import dao.DS;
import dao.pizzas.PizzaDAODataBase;
import dto.Commande;
import dto.Ingredient;
import dto.Pizza;

public class CommandesDAODatabase implements DAOCommandes {

    @Override
    public List<Commande> findAll(){
        List<Commande> all = new ArrayList<>();
        try(Connection con = DS.getConnection()){
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM commandes;");
            Commande commande;
            while (rs.next()) {
                commande = new Commande(rs.getInt(1),rs.getString(2),rs.getDate(3));
                commande.addAllPizzas(findPizzasById(commande.getId()));
                all.add(commande);
            }
            return all;
        } catch (SQLException e) {
            return all;
        }
    }

    public List<Pizza> findPizzasById(int id){
        List<Pizza> all = new ArrayList<>();
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT pizzas.pno,nom,pate,prix_base,quantite FROM pizzas JOIN pizzas_dans_commandes ON(pizzas.pno = pizzas_dans_commandes.pno) WHERE cno=?;");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            Pizza pizza;
            while (rs.next()) {
                pizza = new Pizza(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
                List<Ingredient> ingredientsPizza = new PizzaDAODataBase().findIngredientsById(pizza.getId());
                pizza.addAllIngredients(ingredientsPizza);
                for(int i=0;i<rs.getInt(5);i++){
                    all.add(pizza);
                }
            }
            if (all.size() == 0){                
                return null;
            } else {
                return all;
            }
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public Commande findById(int id) {
        try(Connection con = DS.getConnection()){
            Commande commande = null;
            PreparedStatement ps = con.prepareStatement("SELECT * FROM commandes WHERE cno=?;");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                commande = new Commande(rs.getInt(1),rs.getString(2),rs.getDate(3));
                commande.addAllPizzas(findPizzasById(commande.getId()));
            }
            return commande;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public boolean save(Commande commande) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("INSERT INTO Commandes(nom_utilisateur,jour_creer) VALUES (?,?) RETURNING cno;");
            ps.setString(1, commande.getUserName());
            ps.setDate(2, commande.getDayCreated());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                int generatedKey = rs.getInt(1);
                commande.setId(generatedKey);
                return savePizzas(commande.getId(),commande.getPizzas());
            } else {
                return false;
            }
        } catch (SQLException e) {
            return false;
        }
    }

    private boolean searchNamePizza(String name) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT pno FROM pizzas WHERE nom=? ;");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            return false;
        }
    }

    private Ingredient searchIngredient(String name) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT ino,prix FROM ingredients WHERE nom=? ;");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return new Ingredient(rs.getInt(1),name,rs.getInt(2));
        } catch (SQLException e) {
            return null;
        }
    }

    private boolean searchNameIngredient(String name) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT ino FROM ingredients WHERE nom=? ;");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            return false;
        }
    }

    private boolean saveIngredients(int id,List<Ingredient> ingredients){
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("INSERT INTO ingredients(nom,prix) VALUES (?,?) RETURNING ino ;");
            for(Ingredient ingredient : ingredients){
                if (!searchNameIngredient(ingredient.getName())){
                    try{
                        ps.setString(1, ingredient.getName());
                        ps.setInt(2, ingredient.getPrize());
                        ResultSet rs = ps.executeQuery();
                        if(rs.next()){
                            int generatedKey = rs.getInt(1);
                            ingredient.setId(generatedKey);
                        }
                    } catch (SQLException e){
                        continue;
                    } finally{
                        ps.clearParameters();
                    }
                } else {
                    Ingredient verifIngredient = searchIngredient(ingredient.getName());
                    if (verifIngredient != null){
                        ingredient.setId(verifIngredient.getId());
                        ingredient.setPrize(verifIngredient.getPrize());
                    }
                }
            }
            return saveIngredientsOfPizzas(id, ingredients);
        } catch (SQLException e) {
            return false;
        }
    }

    private boolean saveIngredientsOfPizzas(int id,List<Ingredient> ingredients){
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("INSERT INTO ingredients_par_pizzas(pno,ino) VALUES (?,?) ;");
            for(Ingredient ingredient : ingredients){
                try{
                    ps.setInt(1, id);
                    ps.setInt(2, ingredient.getId());
                    ps.executeUpdate();
                } catch (SQLException e){
                    continue;
                } finally{
                    ps.clearParameters();
                }
            }
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    private boolean savePizzas(int cno,List<Pizza> pizzas){
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("INSERT INTO pizzas(nom,pate,prix_base) VALUES (?,?,?) RETURNING pno ;");
            for(Pizza pizza : pizzas){
                if (!searchNamePizza(pizza.getNom())){
                    saveIngredients(pizza.getId(), pizza.getIngredients());
                    try{
                        ps.setString(1, pizza.getNom());
                        ps.setString(2,pizza.getPate());
                        ps.setInt(3, pizza.getPrixBase());
                        ResultSet rs = ps.executeQuery();
                        if(rs.next()){
                            int generatedKey = rs.getInt(1);
                            pizza.setId(generatedKey);
                        }
                    } catch (SQLException e){
                        continue;
                    } finally{
                        ps.clearParameters();
                    }
                } else {
                    Pizza verif = searchPizza(pizza.getNom());
                    if(verif != null){
                        pizza.setId(verif.getId());
                        pizza.setPate(verif.getPate());
                        pizza.setPrixBase(verif.getPrixBase());
                        loadIngredientOfPizza(pizza);
                    }
                }
            }
            return savePizzasInCommandes(cno, pizzas);
        } catch (SQLException e) {
            return false;
        }
    }

    private void loadIngredientOfPizza(Pizza pizza) {
        List<Ingredient> all = new ArrayList<>();
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT ingredients.ino,nom,prix FROM ingredients JOIN ingredients_par_Pizzas ON(ingredients.ino = ingredients_par_Pizzas.ino) WHERE pno=?;");
            ps.setInt(1, pizza.getId());
            ResultSet rs = ps.executeQuery();
            Ingredient ingredient;
            while (rs.next()) {
                ingredient = new Ingredient(rs.getInt(1),rs.getString(2),rs.getInt(3));
                all.add(ingredient);
            }
            pizza.addAllIngredients(all);
        } catch (SQLException e) {
            pizza.addAllIngredients(all);
        }
    }

    private Pizza searchPizza(String name) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT pno,pate,prix_base FROM pizzas WHERE nom=? ;");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return new Pizza(rs.getInt(1),name,rs.getString(2),rs.getInt(3));
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean savePizzasInCommandes(int cno, List<Pizza> pizzas) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("INSERT INTO pizzas_dans_commandes(cno,pno,quantite) VALUES (?,?,?) ;");
            Map<Pizza,Integer> quantites = recoverQuantiteAndDistinctPizza(pizzas);
            for(Entry<Pizza, Integer> quantitePizza : quantites.entrySet()){
                ps.setInt(1, cno);
                ps.setInt(2, quantitePizza.getKey().getId());
                ps.setInt(3, quantitePizza.getValue());
                ps.addBatch();
            }
            ps.executeBatch();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    private Map<Pizza,Integer> recoverQuantiteAndDistinctPizza(List<Pizza> pizzas) {
        Map<Pizza,Integer> quantitePizza = new HashMap<>();
        Integer quantite;
        for(Pizza pizza : pizzas){
            quantite = quantitePizza.get(pizza);
            if(quantite == null){
                quantitePizza.put(pizza,1);
            } else {
                quantitePizza.put(pizza,quantitePizza.get(pizza)+1);
            }
        }
        return quantitePizza;
    }

    @Override
    public Integer findFinalPrizeById(int id) {
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT SUM(quantite*prix_base)+(SELECT SUM(quantite*prix) FROM pizzas_dans_commandes AS pc JOIN (pizzas AS p JOIN (ingredients_par_pizzas AS ip JOIN ingredients AS i ON(i.ino = ip.ino)) ON(ip.pno = p.pno) ) ON(p.pno = pc.pno) WHERE pc.cno=?) FROM pizzas_dans_commandes AS pc JOIN pizzas AS p ON(p.pno = pc.pno) WHERE pc.cno=? ;");
            ps.setInt(1, id);
            ps.setInt(2, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                if (rs.getInt(1) == 0){
                    return null;
                }
                return rs.getInt(1);
            } else {
                return null;
            }
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public String findUserById(int id) {
        String userName = null;
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT nom_utilisateur FROM commandes WHERE cno=?;");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                userName = rs.getString(1);
            }
            return userName;
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public Date findDateById(int id) {
        Date date = null;
        try(Connection con = DS.getConnection()){
            PreparedStatement ps = con.prepareStatement("SELECT jour_creer FROM commandes WHERE cno=?;");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                date = rs.getDate(1);
            }
            return date;
        } catch (SQLException e) {
            return null;
        }
    }
    
}

package controleurs;

import java.io.IOException;
import java.util.Base64;

import dao.DAOFactory;
import dao.users.DAOUser;
import dto.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/users/*")
public class UserRestAPI extends HttpServlet {
    
    private DAOUser daoUser = DAOFactory.createDAOUser();

    
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String info = req.getPathInfo();

        if (info == null || info.equals("/")){
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            String[] parts = info.split("/");
            if (parts.length == 2 && parts[1].equals("token")){
                User user = recoverUser(req);
                if(user != null && daoUser.userExists(user)){
                    daoUser.addToken(user);
                    Utils.printResponse(user.getToken(), res);
                } else {
                    res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                res.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
    }

    private User recoverUser(HttpServletRequest req){
        String authorization = req.getHeader("Authorization");
        if (authorization == null || !authorization.startsWith("Basic")){
            return null;
        } else {
            String token = authorization.substring("Basic".length()).trim();
            byte[] base64 = Base64.getDecoder().decode(token);
            String[] lm = (new String(base64)).split(":");
            return new User(lm[0],lm[1]);
        }
    }
}

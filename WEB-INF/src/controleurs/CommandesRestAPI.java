package controleurs;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import controleurs.Exception.BadRequestException;
import dao.DAOFactory;
import dao.commandes.DAOCommandes;
import dto.Commande;
import dto.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/commandes/*")
public class CommandesRestAPI extends HttpServlet {


    private DAOCommandes daoCommandes = DAOFactory.createDAOCommandes();


    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String info = req.getPathInfo();

        if (info == null || info.equals("/")){
            List<Commande> allCommande = daoCommandes.findAll();
            Utils.printResponse(allCommande, res);
        } else {
            String[] parts = info.split("/");
            if(Utils.checkIfId(res, parts[1])){
                int id = Integer.parseInt(parts[1]);
                if (parts.length == 2){
                    Commande commande = daoCommandes.findById(id);
                    Utils.printResponse(commande, res);
                } else if (parts.length == 3) {
                    try{
                        Object obj = recoverAttributeOfCommande(id, parts[2]);
                        Utils.printResponse(obj, res);
                    } catch (BadRequestException e){
                        res.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    }
                } else {
                    res.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }
            }
        }
    }

    private Object recoverAttributeOfCommande(int id, String attribute) throws BadRequestException{
        switch (attribute) {
            case "utilisateur":
                return daoCommandes.findUserById(id);
            case "prixfinal":
                return daoCommandes.findFinalPrizeById(id);
            case "jourcreer":
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date dayCreated = daoCommandes.findDateById(id);
                if (dayCreated == null){
                    return null;
                } else {
                    return dateFormat.format(dayCreated);
                }
            case "pizzas":
                return daoCommandes.findPizzasById(id);
            default:
                throw new BadRequestException();
        }
    }


    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        String info = req.getPathInfo();
        if (info != null && !info.equals("/")){
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }

        String authorization = req.getHeader("Authorization");
        if (authorization != null && authorization.startsWith("Bearer")){
            String token = authorization.substring("Bearer".length()).trim();
            if(DAOFactory.createDAOUser().tokenExists(token)){
                Commande commande = Utils.recoverRequestCommande(req);
                boolean asSave = false;
                User user = DAOFactory.createDAOUser().userOfThisToken(token);
                if(user != null){
                    commande.setUserName(user.getNom());
                    asSave = daoCommandes.save(commande); 
                }

                if(asSave){
                    res.setStatus(HttpServletResponse.SC_CREATED);
                    Utils.printResponse(commande, res);
                } else {
                    res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
            } else {
                res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
        }  else {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
    
}

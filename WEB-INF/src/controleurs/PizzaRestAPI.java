package controleurs;

import java.io.IOException;
import java.util.List;

import controleurs.Exception.BadRequestException;
import dao.DAOFactory;
import dao.pizzas.DAOPizza;
import dto.Ingredient;
import dto.Pizza;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/pizzas/*")
public class PizzaRestAPI extends HttpServlet {


    private DAOPizza daoPizza = DAOFactory.createDAOPizza();

    
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String info = req.getPathInfo();

        if (info == null || info.equals("/")){
            List<Pizza> allPizzas = daoPizza.findAll();
            Utils.printResponse(allPizzas, res);
        } else {
            String[] parts = info.split("/");
            if(Utils.checkIfId(res, parts[1])){
                int id = Integer.parseInt(parts[1]);
                if (parts.length == 2){
                    Pizza pizza = daoPizza.findById(id);
                    Utils.printResponse(pizza, res);
                } else if (parts.length == 3) {
                    try{
                        Object obj = recoverAttributeOfPizza(id, parts[2]);
                        Utils.printResponse(obj, res);
                    } catch (BadRequestException e){
                        res.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    }
                } else {
                    res.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }
            }
        }
    }


    private Object recoverAttributeOfPizza(int id, String attribute) throws BadRequestException{
        switch (attribute) {
            case "nom":
                return daoPizza.findNameById(id);
            case "pate":
                return daoPizza.findDoughById(id);
            case "prixbase":
                return daoPizza.findPrizeById(id);
            case "prixfinal":
                return daoPizza.findFinalPrizeById(id);
            case "ingredients":
                return daoPizza.findIngredientsById(id);
            default:
                throw new BadRequestException();
        }
    }


    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        String info = req.getPathInfo();

        if (info == null || info.equals("/")){
            Pizza pizza = Utils.recoverRequestPizza(req);
            boolean asSave = daoPizza.save(pizza);
            if(asSave){
                res.setStatus(HttpServletResponse.SC_CREATED);
                Utils.printResponse(pizza, res);
            } else {
                res.sendError(HttpServletResponse.SC_CONFLICT);
            }
        } else {
            String[] parts = info.split("/");
            if(Utils.checkIfId(res, parts[1])){
                int id = Integer.parseInt(parts[1]);
                if (parts.length == 2){
                    Ingredient ingredient = Utils.recoverRequestIngredient(req);
                    if(daoPizza.addIngredientById(id, ingredient)){
                        Utils.printResponse(ingredient, res);
                    } else {
                        res.sendError(HttpServletResponse.SC_CONFLICT);
                    }
                } else {
                    res.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }
            }
        }
    }

    public void doDelete(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        String info = req.getPathInfo();
        if (info == null) res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        
        String[] parts = info.split("/");
        if(Utils.checkIfId(res, parts[1])){
            int id = Integer.parseInt(parts[1]);
            if (parts.length == 2){
                Pizza pizza = daoPizza.findById(id);
                if(daoPizza.deleteById(id)){
                    Utils.printResponse(pizza, res);
                } else {
                    res.sendError(HttpServletResponse.SC_CONFLICT);
                }
            } else if(parts.length == 3){
                Utils.checkIfId(res, parts[2]);
                int idIngredient = Integer.parseInt(parts[2]);
                Ingredient ingredient = DAOFactory.createDAOIngredients().findById(idIngredient);
                if (daoPizza.removeIngredientById(id,idIngredient)){
                    Utils.printResponse(ingredient, res);
                } else {
                    res.sendError(HttpServletResponse.SC_CONFLICT);
                }
            } else {
                res.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
    }

    public void doPut(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String info = req.getPathInfo();
        Pizza pizza = Utils.recoverRequestPizza(req);
        if (info == null || info.equals("/")){ 
            res.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        } else if (pizza == null){
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            String[] parts = info.split("/");
            if(parts.length == 2 && Utils.checkIfId(res, parts[1])){
                int id = Integer.parseInt(parts[1]);
                boolean asSave = daoPizza.modifAllById(id,pizza);
                if(asSave){
                    pizza = daoPizza.findById(id);
                    Utils.printResponse(pizza, res);
                } else {
                    res.sendError(HttpServletResponse.SC_CONFLICT);
                }
            }
        }
    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (req.getMethod().equalsIgnoreCase("GET")) {
            super.service(req, res);
        } else {
            if(Utils.isAutorized(req)){
                if (req.getMethod().equalsIgnoreCase("PATCH")) {
                    doPatch(req, res);
                } else {
                    super.service(req, res);
                }
            } else {
                res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
        }
    }

    public void doPatch(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        String info = req.getPathInfo();
        Pizza pizza = Utils.recoverRequestPizza(req);
        if (info == null || info.equals("/")){ 
            res.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        } else if (pizza == null){
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            String[] parts = info.split("/");
            if(parts.length == 2 && Utils.checkIfId(res, parts[1])){
                int id = Integer.parseInt(parts[1]);
                boolean asSave = daoPizza.modifById(id,pizza);
                if(asSave){
                    pizza = daoPizza.findById(id);
                    Utils.printResponse(pizza, res);
                } else {
                    res.sendError(HttpServletResponse.SC_CONFLICT);
                }
            }
        }
    }
}

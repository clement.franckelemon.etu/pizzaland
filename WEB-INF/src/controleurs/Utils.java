package controleurs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.DAOFactory;
import dto.Commande;
import dto.Ingredient;
import dto.Pizza;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class Utils {

    private static ObjectMapper objectMapper = new ObjectMapper();

    protected static void printResponse(Object object,HttpServletResponse res) throws IOException{
        if(object == null){
            res.sendError(HttpServletResponse.SC_NOT_FOUND);
        } else {
            res.setContentType("application/json;charset=UTF-8");
            PrintWriter out = res.getWriter();
            String jsonstring = objectMapper.writeValueAsString(object);
            out.println(jsonstring);
            out.close();
        }
    }

    protected static boolean checkIfId(HttpServletResponse res,String id) throws IOException{
        try{
            Integer.parseInt(id);
            return true;
        } catch (NumberFormatException exception){
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return false;
        }
    }

    private static String recoverRequestDate(HttpServletRequest req) throws IOException{
        StringBuilder data = new StringBuilder();
        BufferedReader reader = req.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            data.append(line);
        }
        return data.toString();
    }

    protected static Ingredient recoverRequestIngredient(HttpServletRequest req) throws IOException{
        String data = recoverRequestDate(req);
        try{
            Ingredient ingredient = objectMapper.readValue(data, Ingredient.class);
            return ingredient;
        } catch(Exception e){
            return null;
        }
    }

    protected static Pizza recoverRequestPizza(HttpServletRequest req) throws IOException{
        String data = recoverRequestDate(req);
        try{
            Pizza pizza = objectMapper.readValue(data.toString(), Pizza.class);
            return pizza;
        } catch(Exception e){
            return null;
        }
    }

    protected static Commande recoverRequestCommande(HttpServletRequest req) throws IOException{
        String data = recoverRequestDate(req);
        try{
            Commande commande = objectMapper.readValue(data.toString(), Commande.class);
            return commande;
        } catch(Exception e){
            return null;
        }
    }

    protected static boolean isAutorized(HttpServletRequest req) throws IOException{
        String authorization = req.getHeader("Authorization");
        if (authorization == null || !authorization.startsWith("Bearer")){
            return false;
        }
        String token = authorization.substring("Bearer".length()).trim();
        if(token == null){
            return false;
        } else {
            return DAOFactory.createDAOUser().tokenExists(token);
        }
    }
}

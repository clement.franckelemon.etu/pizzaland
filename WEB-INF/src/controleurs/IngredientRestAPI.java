package controleurs;

import java.io.IOException;
import java.util.List;

import controleurs.Exception.BadRequestException;
import dao.DAOFactory;
import dao.ingredients.DAOIngredient;
import dto.Ingredient;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/ingredients/*")
public class IngredientRestAPI extends HttpServlet {


    private DAOIngredient daoIngredient = DAOFactory.createDAOIngredients();


    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String info = req.getPathInfo();

        if (info == null || info.equals("/")){
            List<Ingredient> allIngredients = daoIngredient.findAll();
            Utils.printResponse(allIngredients, res);
        } else {
            String[] parts = info.split("/");
            if(Utils.checkIfId(res, parts[1])){
                int id = Integer.parseInt(parts[1]);
                if (parts.length == 2){
                    Ingredient ingredient = daoIngredient.findById(id);
                    Utils.printResponse(ingredient, res);
                } else if (parts.length == 3) {
                    try{
                        Object ingredientAttribut = recoverAttributeOfIngredient(id,parts[2]);
                        Utils.printResponse(ingredientAttribut, res);
                    } catch (BadRequestException e){
                        res.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    }
                } else {
                    res.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }
            }
        }
    }


    private Object recoverAttributeOfIngredient(int id, String attribute) throws BadRequestException{
        switch (attribute) {
            case "nom":
                return daoIngredient.findNameById(id);
            case "prix":
                return daoIngredient.findPrizeById(id);
            default:
                throw new BadRequestException();
        }
    }


    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        String info = req.getPathInfo();
        if (info != null && !info.equals("/")){
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }

        Ingredient ingredient = Utils.recoverRequestIngredient(req);
        if (ingredient.getName() == null || ingredient.getName().equals("")){
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            boolean asSave = daoIngredient.save(ingredient);

            if(asSave){
                res.setStatus(HttpServletResponse.SC_CREATED);
                Utils.printResponse(ingredient, res);
            } else {
                res.sendError(HttpServletResponse.SC_CONFLICT);
            }
        }
    }

    
    public void doDelete(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        String info = req.getPathInfo();
        if (info == null) res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        
        String[] parts = info.split("/");
        if (parts.length == 2){
            if(Utils.checkIfId(res, parts[1])){
                int id = Integer.parseInt(parts[1]);
                Ingredient ingredient = daoIngredient.findById(id);
                if (daoIngredient.deleteById(id)){
                    Utils.printResponse(ingredient, res);
                } else {
                    res.sendError(HttpServletResponse.SC_CONFLICT);
                }
            }
        }
    }

    public void doPut(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String info = req.getPathInfo();
        Ingredient ingredient = Utils.recoverRequestIngredient(req);
        if (info == null || info.equals("/")){ 
            res.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        } else if (ingredient == null){
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            String[] parts = info.split("/");
            if(parts.length == 2 && Utils.checkIfId(res, parts[1])){
                int id = Integer.parseInt(parts[1]);
                boolean asSave = daoIngredient.modifAllById(id,ingredient);
                if(asSave){
                    ingredient = daoIngredient.findById(id);
                    Utils.printResponse(ingredient, res);
                } else {
                    res.sendError(HttpServletResponse.SC_CONFLICT);
                }
            }
        }
    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (req.getMethod().equalsIgnoreCase("GET")) {
            super.service(req, res);
        } else {
            if(Utils.isAutorized(req)){
                if (req.getMethod().equalsIgnoreCase("PATCH")) {
                    doPatch(req, res);
                } else {
                    super.service(req, res);
                }
            } else {
                res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
        }
    }

    public void doPatch(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String info = req.getPathInfo();
        Ingredient ingredient = Utils.recoverRequestIngredient(req);
        if (info == null || info.equals("/")){ 
            res.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        } else if (ingredient == null){
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            String[] parts = info.split("/");
            if(parts.length == 2 && Utils.checkIfId(res, parts[1])){
                int id = Integer.parseInt(parts[1]);
                boolean asSave = daoIngredient.modifById(id,ingredient);
                if(asSave){
                    ingredient = daoIngredient.findById(id);
                    Utils.printResponse(ingredient, res);
                } else {
                    res.sendError(HttpServletResponse.SC_CONFLICT);
                }
            }
        }
    }
}
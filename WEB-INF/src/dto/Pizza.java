package dto;

import java.util.ArrayList;
import java.util.List;

public class Pizza {
    
    private int id;
    private String nom;
    private String pate;
    private int prixBase;
    private List<Ingredient> ingredients;

    public Pizza(int id,String nom, String pate, int prixBase) {
        this.id = id;
        this.nom = nom;
        this.pate = pate;
        this.prixBase = prixBase;
        this.ingredients = new ArrayList<>();
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pizza(){
        this.ingredients = new ArrayList<>();
    }

    public int prixFinal(){
        int somme = prixBase;
        for (Ingredient ingredient : ingredients){
            somme += ingredient.getPrize();
        }
        return somme;
    }

    public boolean addIngredient(Ingredient ingredient){
        return ingredients.add(ingredient);
    }

    public boolean removeIngredient(int idIngredient){
        boolean isRemove = false;
        int idx = 0;
        int idTmp;
        while (idx < ingredients.size() && !isRemove) {
            idTmp = ingredients.get(idx).getId();
            isRemove = (idTmp == idIngredient);
            if(isRemove) ingredients.remove(idx);
            idx++;
        }
        return isRemove;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPate() {
        return pate;
    }

    public void setPate(String pate) {
        this.pate = pate;
    }

    public int getPrixBase() {
        return prixBase;
    }

    public void setPrixBase(int prixBase) {
        this.prixBase = prixBase;
    }

    public int getId() {
        return id;
    }

    public void addAllIngredients(List<Ingredient> ingredients) {
        for(Ingredient ingredient : ingredients){
            this.addIngredient(ingredient);
        }
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pizza other = (Pizza) obj;
        if (id != other.id)
            return false;
        return true;
    }

    

}

package dto;

public class Ingredient {
    
    private int id;
    private String name;
    private int prize;

    public Ingredient(int id, String name, int prize) {
        this.id = id;
        this.name = name;
        this.prize = prize;
    }

    public Ingredient(int id, String name) {
        this(id,name,0);
    }
    
    public Ingredient() {

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrize() {
        return prize;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }

    public void setId(int id) {
        this.id = id;
    }

}

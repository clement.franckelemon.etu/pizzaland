package dto;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Commande {
    
    private int id;
    private String userName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date dayCreated;
    private List<Pizza> pizzas;

    public Commande(){

    }

    public void setId(int id) {
        this.id = id;
    }

    public Commande(int id, String userName, Date dayCreated) {
        this.id = id;
        this.userName = userName;
        this.dayCreated = dayCreated;
        this.pizzas = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    public Date getDayCreated() {
        // la récupération de la date me renvoie toujours le jour précédent au jour donné, j'ajoute donc un jour
        LocalDate nextDay = dayCreated.toLocalDate().plusDays(1);
        return Date.valueOf(nextDay);
    }

    public List<Pizza> getPizzas() {
        return pizzas;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setDayCreated(Date dayCreated) {
        this.dayCreated = dayCreated;
    }

    public boolean addPizza(Pizza pizza){
        return this.pizzas.add(pizza);
    }

    public void addAllPizzas(List<Pizza> pizzas) {
        for(Pizza pizza : pizzas){
            this.addPizza(pizza);
        }
    }

    public int finalPrize(){
        int somme = 0;
        for (Pizza pizza : pizzas){
            somme += pizza.prixFinal();
        }
        return somme;
    }

}

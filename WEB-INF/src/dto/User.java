package dto;

public class User {
    private String nom;
    private String mdp;
    private String token;
    
    public User(String nom, String mdp, String token) {
        this.nom = nom;
        this.mdp = mdp;
        this.token = token;
    }

    public User(String nom, String mdp) {
        this(nom, mdp, null);
    }

    public String getNom() {
        return nom;
    }

    public String getMdp() {
        return mdp;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    

    
}

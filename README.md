# PIZZALAND 🍕

Pizzaland est une API de gestion de pizzeria.

## Documentation 📚

Vous pourrez retrouver toute la documentation dans le dossier **Documentation** dans le format de votre choix (PDF/MarkDown). 

## Initialisation de la Base de Données 🔧

Vous pourrez retrouver un fichier nommé **init.sql** déstinié à l'initialisation et à la réinitinialisation de la base de données.

## Bruno 🐶

Vous pourrez retrouver tous les requêtes les plus importantes avec **Bruno** dans le dossier pizzaland. Les requêtes sont ranger de tel façon à tester à la suite toutes les d'erreurs et les cas standard d'utilisation de l'API Pizzaland.

## Auteur ✏️
- Franckelemon Clément
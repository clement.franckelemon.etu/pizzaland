/* Efface toutes les tables */
DROP TABLE IF EXISTS pizzas_dans_commandes;
DROP TABLE IF EXISTS commandes;
DROP TABLE IF EXISTS ingredients_par_pizzas;
DROP TABLE IF EXISTS ingredients;
DROP TABLE IF EXISTS pizzas;
DROP TABLE IF EXISTS utilisateurs;



/* Crée et préremplie la table des ingredients */
CREATE TABLE ingredients(
    ino SERIAL PRIMARY KEY, 
    nom VARCHAR(64), 
    prix INT
);

INSERT INTO ingredients(nom,prix)
VALUES ('salade',1),
('ananas',3),
('jambon',5),
('fromage',4),
('banane',NULL);



/* Crée et préremplie la table des utilisateurs */
CREATE TABLE utilisateurs(
    nom VARCHAR(64) PRIMARY KEY,
    mdp VARCHAR(256),
    token VARCHAR(512)
);

INSERT INTO utilisateurs
VALUES ('toto','toto',NULL),
('tata','toto',NULL),
('titi','tata',NULL),
('jean','jean',NULL),
('MrJambon','fromage',NULL);



/* Crée et préremplie la table des pizzas */
CREATE TABLE pizzas(
    pno SERIAL PRIMARY KEY, 
    nom VARCHAR(64), 
    pate VARCHAR(64), 
    prix_base INT
);

INSERT INTO pizzas(nom,pate,prix_base)
VALUES ('reine','moyenne',5),
('ananas','fine',7),
('4 fromage','epaise',5);



/* Crée et préremplie la table des ingredients composent une pizzas */
CREATE TABLE ingredients_par_pizzas(
    pno INT, 
    ino INT,
    CONSTRAINT pk_ingredients_par_pizzas PRIMARY KEY (pno, ino), 
    CONSTRAINT fk_pizzas FOREIGN KEY (pno) REFERENCES pizzas(pno) ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT fk_ingredients FOREIGN KEY (ino) REFERENCES ingredients(ino) ON DELETE RESTRICT ON UPDATE CASCADE
);

INSERT INTO ingredients_par_pizzas(pno,ino)
VALUES (1,3),
(1,4),
(2,4),
(2,2),
(3,4);



/* Crée et préremplie la table des commandes */
CREATE TABLE commandes(
    cno SERIAL,
    nom_utilisateur VARCHAR(64),
    jour_creer DATE,
    CONSTRAINT pk_commandes PRIMARY KEY (cno),
    CONSTRAINT fk_utilisateur FOREIGN KEY (nom_utilisateur) REFERENCES utilisateurs(nom) ON DELETE RESTRICT ON UPDATE CASCADE
);

INSERT INTO commandes(nom_utilisateur,jour_creer)
VALUES ('toto','2023-12-31'),
('tata','2024-01-12'),
('titi','2024-02-24');



/* Crée et préremplie la table des pizzasInCommandes representant la liste des pizzas dans une commande */
CREATE TABLE pizzas_dans_commandes(
    cno INT, 
    pno INT,
    quantite INT,
    CONSTRAINT invalide_quantite CHECK (quantite > 0),
    CONSTRAINT pk_pizzas_dans_commandes PRIMARY KEY (cno,pno), 
    CONSTRAINT fk_pizzas FOREIGN KEY (pno) REFERENCES pizzas(pno) ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT fk_commandes FOREIGN KEY (cno) REFERENCES commandes(cno) ON DELETE RESTRICT ON UPDATE CASCADE
);

INSERT INTO pizzas_dans_commandes(cno,pno,quantite)
VALUES (1,1,3),
(1,3,2),
(2,2,1),
(2,3,2),
(3,1,1);
